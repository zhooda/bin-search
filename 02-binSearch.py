"""
T: 3110 Binary Search Example 1
A: Zeeshan Hooda
D: 2019-02-08
"""
import random as r
# print(f'[REQUEST] = {inv}\n[RANGE] = {len(arr)} ')

def midCheck(arr, inv):
    while len(arr) >= 1:
        m = len(arr) // 2

        if inv == arr[m]:
            return arr[m]

        elif inv > arr[m]:
            arr = arr[m+1:len(arr)]

        else:
            arr = arr[0:m-1]

lizt = []

for i in range(100000):
    if r.randrange(1000) == 1:
        print('[MAKING TEST DATA]')
    # GUARANTEED NUMBER FOR TESTING
    if i == 4981:
        lizt.append(i)
    # RANDOM LIST GEN FOR TESTING
    if r.randrange(2) == 1:
        lizt.append(i)

print('\n[DATA READY]\n\n[GUARANTEED VALUE] = 4981')
req = int(input('Search value: '))

print(f'Found {midCheck(lizt, req)} in set of {len(lizt)} items.')
